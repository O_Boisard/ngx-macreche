import { of as observableOf,  Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class User {
  id: number;
  gender: string;
  firstname: string;
  lastname: string;
  picture: string;
  birthday: string;
  username: string;
  email: string;
  constructor() {
  }
}

export class Users {

}

@Injectable()
export class UserService {

  basepath : string = 'api/user';

  usersSubject: Subject<User[]> = new Subject<User[]>();
  userSubject: Subject<User> = new Subject<User>();

  constructor(private http: HttpClient) {
  }

  getUsers(): void {
    this.http.get<User[]>(this.basepath , {}).subscribe(users => {
      this.usersSubject.next(users);
    });
  }

  getUserById(id): void {
    this.http.get<User>(this.basepath + '?id=' + id, {}).subscribe(user => {
      this.userSubject.next(user[0]);
    });
  }
}