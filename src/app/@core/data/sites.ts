import { of as observableOf,  Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Site {
  id: number;
  name: string;
  address: string;
  cp: string;
  city: string;
  idcompany: number;
  constructor() {
  }
}

export class Sites {

}

@Injectable()
export class SiteService {

  basepath : string = 'api/site';
  userSubject: Subject<Site[]> = new Subject<Site[]>();

  constructor(private http: HttpClient) {
  }

  getSites(): void {
    this.http.get<Site[]>(this.basepath , {}).subscribe(result => {
      this.userSubject.next(result);
    });
  }
}