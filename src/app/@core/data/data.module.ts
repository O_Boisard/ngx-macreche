import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './users';
import { ChildService } from './children';
import { CompanyService } from './companies';
import { SiteService } from './sites';
import { SectionService } from './sections';

const SERVICES = [
  UserService,
  ChildService,
  CompanyService,
  SiteService,
  SectionService
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
