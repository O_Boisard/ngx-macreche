import { of as observableOf,  Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Section {
  id: number;
  name: string;
  idsite: number;
  constructor() {
  }
}

export class Sections {

}

@Injectable()
export class SectionService {

  basepath : string = 'api/section';
  userSubject: Subject<Section[]> = new Subject<Section[]>();

  constructor(private http: HttpClient) {
  }

  getSections(): void {
    this.http.get<Section[]>(this.basepath , {}).subscribe(result => {
      this.userSubject.next(result);
    });
  }
}