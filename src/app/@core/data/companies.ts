import { of as observableOf,  Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Company {
  id: number;
  name: string;
  address: string;
  cp: string;
  city: string;
  constructor() {
  }
}

export class Companies {

}

@Injectable()
export class CompanyService {

  basepath : string = 'api/company';
  userSubject: Subject<Company[]> = new Subject<Company[]>();

  constructor(private http: HttpClient) {
  }

  getCompanies(): void {
    this.http.get<Company[]>(this.basepath , {}).subscribe(result => {
      this.userSubject.next(result);
    });
  }
}