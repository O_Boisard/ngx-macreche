import { of as observableOf,  Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Child {
  id: number;
  gender: string;
  firstname: string;
  lastname: string;
  picture: string;
  birthday: string;
  constructor() {
  }
}

export class Children {

}

@Injectable()
export class ChildService {

  basepath : string = 'api/child';
  userSubject: Subject<Child[]> = new Subject<Child[]>();

  constructor(private http: HttpClient) {
  }

  getChildren(): void {
    this.http.get<Child[]>(this.basepath , {}).subscribe(result => {
      this.userSubject.next(result);
    });
  }
}