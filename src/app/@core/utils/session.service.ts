import { User, UserService } from '../data/users';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export class Session {
    user: User;
    constructor() {

    }
}

@Injectable()
export class SessionService {

    private isLoaded: any;
    private session:Session = new Session();

    subject: Subject<Session> = new Subject<Session>();

    constructor(
        private userService: UserService
    ) {
        this.isLoaded = {
            user: false
        };
    }

    getUser(): void {

        this.userService.userSubject.subscribe(user => {
            this.session.user = user;
            this.subject.next(this.session);
        });

        if(!this.isLoaded.user) {
            this.userService.getUserById('1');
            this.isLoaded.user = true;
        }
    }
}