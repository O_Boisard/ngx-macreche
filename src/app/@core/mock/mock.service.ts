import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ChildData } from './children.data';
import { UserData } from './users.data';
import { CompanyData } from './company.data';
import { SiteData } from './site.data';
import { SectionData } from './section.data';

@Injectable({
  providedIn: 'root'
})
export class MockService implements InMemoryDbService {
  createDb(): any {
    return {
      child: ChildData,
      user: UserData,
      company: CompanyData,
      site: SiteData,
      section: SectionData
    };
  }
}
