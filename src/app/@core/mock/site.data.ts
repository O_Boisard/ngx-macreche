export const SiteData = [
{
  id: 1,
  name: "Les bobinos",
  address: "10 r Gabrielle Josserand",
  cp: "93500",
  city: "PANTIN",
  idcompany: 1
},
{
  id: 2,
  name: "Mirabelle",
  address: "6 Place Léon Blum",
  cp: "92290",
  city: "CHATENAY MALABRY",
  idcompany: 1
},
{
  id: 3,
  name: "Scoubidous",
  address: "8 Rue Paul Bert",
  cp: "93300",
  city: "AUBERVILLIERS",
  idcompany: 1
}];
