// Angular
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { of as observableOf } from 'rxjs';

// Nebular
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { NbAuthModule, NbDummyAuthStrategy } from '@nebular/auth';

// Local
import { throwIfAlreadyLoaded } from './module-import-guard';

// Module
import { DataModule } from './data/data.module';

// Mock
import { MockService } from './mock/mock.service';

import {
  AnalyticsService,
  LayoutService,
  PlayerService,
  StateService,
  SessionService
} from './utils';

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...DataModule.forRoot().providers,
  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,
  {
    provide: NbRoleProvider,
    useClass: NbSimpleRoleProvider
  },
  AnalyticsService,
  LayoutService,
  PlayerService,
  StateService,
  SessionService
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientInMemoryWebApiModule.forRoot(MockService)
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
