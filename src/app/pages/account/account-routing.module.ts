import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './account.component';
import { InformationComponent } from './information/information.component';
import { PasswordComponent } from './password/password.component';

const routes: Routes = [{
  path: '',
  component: AccountComponent,
  children: [
    {
      path: 'information',
      component: InformationComponent,
    },
    {
      path: 'password',
      component: PasswordComponent,
    }
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BdRoutingModule { }

export const routedComponents = [
  AccountComponent,
  InformationComponent,
  PasswordComponent,
];