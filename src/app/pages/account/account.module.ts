import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@themes/themes.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BdRoutingModule, routedComponents } from './account-routing.module';
import { AccountComponent } from './account.component';

const COMPONENTS = [
  ...routedComponents,
  AccountComponent,
];

const ENTRY_COMPONENTS = [
];

@NgModule({
  imports: [
    ThemeModule,
    BdRoutingModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class AccountModule { }
