import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pages-account-information',
  styleUrls: ['./information.component.scss'],
  templateUrl: './information.component.html',
})
export class InformationComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
