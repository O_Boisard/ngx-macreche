import { NgModule } from '@angular/core';
// Module
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@themes/themes.module';
import { HomeModule } from './home/home.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
// Component
import { PagesComponent } from './pages.component';

const PAGES_MODULES = [
  PagesRoutingModule,
  ThemeModule,
  HomeModule,
  MiscellaneousModule,
];

const PAGES_COMPONENTS = [
  PagesComponent
];

@NgModule({
  imports: [
    ...PAGES_MODULES
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule { }
