import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
// Module
import { AccountModule } from './account/account.module';
import { BdModule } from './bd/bd.module';
// Component
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'home',
    component: HomeComponent,
  }, {
    path: 'account',
    loadChildren: () => AccountModule,
  }, {
    path: 'bd',
    loadChildren: () => BdModule,
  }, {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
