import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'nb-home',
    link: '/pages/home',
    home: true,
  },
  {
    title: 'Mon compte',
    icon: 'nb-tables',
    children: [
      {
        title: 'Mes informations',
        link: '/pages/account/information',
      },
      {
        title: 'Mon mot de passe',
        link: '/pages/account/password',
      },
    ],
  },
  {
    title: 'Base de données',
    icon: 'nb-tables',
    children: [
      {
        title: 'Les utilisateurs',
        link: '/pages/bd/users',
      },
      {
        title: 'Les enfants',
        link: '/pages/bd/children',
      },
      {
        title: 'Les compagnies',
        link: '/pages/bd/companies',
      },
      {
        title: 'Les sites',
        link: '/pages/bd/sites',
      },
      {
        title: 'Les sections',
        link: '/pages/bd/sections',
      },
    ],
  },
];
