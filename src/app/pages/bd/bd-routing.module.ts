import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BdComponent } from './bd.component';
import { UsersComponent } from './users/users.component';
import { ChildrenComponent } from './children/children.component';
import { CompaniesComponent } from './companies/companies.component';
import { SitesComponent } from './sites/sites.component';
import { SectionsComponent } from './sections/sections.component';

const routes: Routes = [{
  path: '',
  component: BdComponent,
  children: [
    {
      path: 'users',
      component: UsersComponent,
    },
    {
      path: 'children',
      component: ChildrenComponent,
    },
    {
      path: 'companies',
      component: CompaniesComponent,
    },
    {
      path: 'sites',
      component: SitesComponent,
    },
    {
      path: 'sections',
      component: SectionsComponent,
    }
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BdRoutingModule { }

export const routedComponents = [
  BdComponent,
  UsersComponent,
  ChildrenComponent,
  CompaniesComponent,
  SitesComponent,
  SectionsComponent
];