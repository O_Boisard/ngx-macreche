import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { CompanyService } from '../../../@core/data';
import { SETTINGS } from './companies.settings';

@Component({
  selector: 'pages-bd-companies',
  styleUrls: ['./companies.component.scss'],
  templateUrl: './companies.component.html',
})
export class CompaniesComponent implements OnInit {

  settings = SETTINGS;

  source: LocalDataSource = new LocalDataSource();

  constructor(private companyService: CompanyService) {
    this.source.load([]);
  }

  ngOnInit() {
    this.source.setSort([{ field: 'id', direction: 'asc' }]);
    this.companyService.userSubject.subscribe(companies => {
      this.source.load(companies);
    });
    this.companyService.getCompanies();
  }
}
