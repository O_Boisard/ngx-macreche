import { SETTINGS_SIMPLE_TABLE as SETTINGS_BASE } from '../../../@themes/settings';

export const SETTINGS: any = Object.assign({}, SETTINGS_BASE, {
    columns: {
        id: {
            title: 'ID',
            type: 'number',
            editable: false,
            filter: true
        },
        name: {
            title: 'Name',
            type: 'string',
        },
        address: {
            title: 'Adresse',
            type: 'string',
        },
        cp: {
            title: 'Code postal',
            type: 'string'
        },
        city: {
            title: 'Ville',
            type: 'string'
        }
    }
});