import { BirthdayRenderComponent } from '../../../@themes/components';
import { SETTINGS_SIMPLE_TABLE as SETTINGS_BASE } from '../../../@themes/settings';

export const SETTINGS: any = Object.assign({}, SETTINGS_BASE, {
    columns: {
        id: {
            title: 'ID',
            type: 'number',
        },
        gender: {
            title: 'Gender',
            type: 'string',
        },
        firstname: {
            title: 'First Name',
            type: 'string',
        },
        lastname: {
            title: 'Last Name',
            type: 'string',
        },
        username: {
            title: 'Username',
            type: 'string',
        },
        email: {
            title: 'E-mail',
            type: 'string',
        },
        birthday: {
            title: 'Birthday',
            type: 'custom',
            renderComponent: BirthdayRenderComponent,
        }
    }
});