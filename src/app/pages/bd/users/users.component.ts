import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { UserService } from '../../../@core/data';
import { SETTINGS } from './users.settings';

@Component({
  selector: 'pages-bd-users',
  styleUrls: ['./users.component.scss'],
  templateUrl: './users.component.html',
})
export class UsersComponent implements OnInit {

  settings = SETTINGS;

  source: LocalDataSource = new LocalDataSource();

  constructor(private userService: UserService) {
    this.source.load([]);
  }

  ngOnInit() {
    this.userService.usersSubject.subscribe(users => {
      this.source.load(users);
    });
    this.userService.getUsers();
  }
}
