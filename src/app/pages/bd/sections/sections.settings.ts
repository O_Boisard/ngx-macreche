import { SETTINGS_SIMPLE_TABLE as SETTINGS_BASE } from '../../../@themes/settings';

export const SETTINGS: any = Object.assign({}, SETTINGS_BASE, {
    columns: {
        id: {
            title: 'ID',
            type: 'number',
            editable: false,
            filter: true
        },
        name: {
            title: 'Name',
            type: 'string',
        },
        idsite: {
            title: 'Site ID',
            type: 'number'
        }
    }
});