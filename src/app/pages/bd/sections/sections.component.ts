import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { SectionService } from '../../../@core/data';
import { SETTINGS } from './sections.settings';

@Component({
  selector: 'pages-bd-sections',
  styleUrls: ['./sections.component.scss'],
  templateUrl: './sections.component.html',
})
export class SectionsComponent implements OnInit {

  settings = SETTINGS;

  source: LocalDataSource = new LocalDataSource();

  constructor(private sectionService: SectionService) {
    this.source.load([]);
  }

  ngOnInit() {
    this.source.setSort([{ field: 'id', direction: 'asc' }]);
    this.sectionService.userSubject.subscribe(companies => {
      this.source.load(companies);
    });
    this.sectionService.getSections();
  }
}
