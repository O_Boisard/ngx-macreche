import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@themes/themes.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BdRoutingModule, routedComponents } from './bd-routing.module';
import { BdComponent } from './bd.component';

const COMPONENTS = [
  ...routedComponents,
  BdComponent,
];

const ENTRY_COMPONENTS = [
];

@NgModule({
  imports: [
    ThemeModule,
    BdRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class BdModule { }
