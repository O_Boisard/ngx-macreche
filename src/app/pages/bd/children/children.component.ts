import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { ChildService } from '../../../@core/data';
import { SETTINGS } from './children.settings';

@Component({
  selector: 'pages-bd-children',
  styleUrls: ['./children.component.scss'],
  templateUrl: './children.component.html',
})
export class ChildrenComponent implements OnInit {

  settings = SETTINGS;

  source: LocalDataSource = new LocalDataSource();

  constructor(private childService: ChildService) {
    this.source.load([]);
  }

  ngOnInit() {
    this.source.setSort([{ field: 'id', direction: 'asc' }]);
    this.childService.userSubject.subscribe(children => {
      this.source.load(children);
    });
    this.childService.getChildren();
  }
}
