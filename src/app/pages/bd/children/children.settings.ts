import { AgeByDateRenderComponent } from '../../../@themes/components';
import { SETTINGS_SIMPLE_TABLE as SETTINGS_BASE } from '../../../@themes/settings';

export const SETTINGS: any = Object.assign({}, SETTINGS_BASE, {
    columns: {
        id: {
            title: 'ID',
            type: 'number',
            editable: false,
            filter: true
        },
        gender: {
            title: 'Gender',
            type: 'string',
            editor: {
                type: 'list',
                config: {
                    selectText: 'Select...',
                    list: [
                        { value: 'M', title: 'Homme' },
                        { value: 'F', title: 'Femme' }
                    ]
                }
            }
        },
        firstname: {
            title: 'First Name',
            type: 'string',
        },
        lastname: {
            title: 'Last Name',
            type: 'string',
        },
        birthday: {
            title: 'Age',
            type: 'custom',
            renderComponent: AgeByDateRenderComponent,
        }
    }
});