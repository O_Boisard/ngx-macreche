import { Component, OnInit } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { SiteService } from '../../../@core/data';
import { SETTINGS } from './sites.settings';

@Component({
  selector: 'pages-bd-sites',
  styleUrls: ['./sites.component.scss'],
  templateUrl: './sites.component.html',
})
export class SitesComponent implements OnInit {

  settings = SETTINGS;

  source: LocalDataSource = new LocalDataSource();

  constructor(private siteService: SiteService) {
    this.source.load([]);
  }

  ngOnInit() {
    this.source.setSort([{ field: 'id', direction: 'asc' }]);
    this.siteService.userSubject.subscribe(companies => {
      this.source.load(companies);
    });
    this.siteService.getSites();
  }
}
