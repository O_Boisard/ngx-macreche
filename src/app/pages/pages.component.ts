import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  constructor(
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle("IEPC");
  }

  menu = MENU_ITEMS;
}
