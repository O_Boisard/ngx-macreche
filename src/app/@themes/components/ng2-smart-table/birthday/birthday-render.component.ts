import { Component, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import * as moment from 'moment';

@Component({
  template: `
    {{renderValue}}
  `,
})
export class BirthdayRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    if(this.value && this.value !== undefined) {
      moment.locale('fr');
      this.renderValue = moment(this.value).format('dddd D MMMM YYYY');
    } else {
      this.renderValue = null;
    }
  }

}