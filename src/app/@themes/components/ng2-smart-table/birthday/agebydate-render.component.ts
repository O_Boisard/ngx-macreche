import { Component, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import * as moment from 'moment';

@Component({
  template: `
    {{renderValue}}
  `,
})
export class AgeByDateRenderComponent implements ViewCell, OnInit {

  renderValue: number;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    if(this.value && this.value !== undefined) {
      moment.locale('fr');
      this.renderValue = moment().diff(moment(this.value), 'years');
    } else {
      this.renderValue = null;
    }
  }

}