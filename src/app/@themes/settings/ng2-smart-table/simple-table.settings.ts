export const SETTINGS_SIMPLE_TABLE: any = {
  actions: {
    add: false,
    edit: false,
    delete: false
  },
  pager: {
    display: true,
    perPage: 5
  },
  mode: 'internal',
  noDataMessage: 'Pas d\'enfants référencés',
  filter: true,
  hideHeader: false,
  hideSubHeader: true
};