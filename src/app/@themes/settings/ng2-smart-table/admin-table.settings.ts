import { SETTINGS_SIMPLE_TABLE as SETTINGS_BASE  } from './simple-table.settings';

export const SETTINGS_ADMIN_TABLE: any = Object.assign({}, SETTINGS_BASE, {
  add: {
    addButtonContent: '<i class="nb-plus"></i>',
    createButtonContent: '<i class="nb-checkmark"></i>',
    cancelButtonContent: '<i class="nb-close"></i>',
  },
  edit: {
    editButtonContent: '<i class="nb-edit"></i>',
    saveButtonContent: '<i class="nb-checkmark"></i>',
    cancelButtonContent: '<i class="nb-close"></i>',
  },
  delete: {
    deleteButtonContent: '<i class="nb-trash"></i>',
    confirmDelete: true,
  },
  actions: {
    add: true,
    edit: true,
    delete: true,
    position: 'right'
  },
  hideSubHeader: false
});